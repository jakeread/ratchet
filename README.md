# Ratchet

## New Ratchet

It's 2021 and I'm taking this machine on a redux. The desires are the same: a shopbot replacement bred with some kind of Zund. A generalist that will happily devour 1" plywood milling jobs, some light ALU milling, and also elegantly knife-cut patterns for custom jeans and net shape composites. 

Big goals are to include BLDC drives, make this easy to build with just small format CNC and keep the budget under $5k... stretch to $3k, will see. 

Drives are likely to be 'real' timing belt, a-la AT5 profile, steel core, big thickness. 

Likely fabbed in thicc phenolic, epoxy to join bits.

This is a sketch for now but [check the dev log](log/ratchet-log.md)

![sketch](log/2021-01-08_ratchet-sketch.jpg)
![sketch](log/2021-01-08_ratchet-sketch.png)

## Old Ratchet

[previous effort](old-ratchet/)  

![img](old-ratchet/log/2020-05-31_ratchet-hip.jpg)

## Bootstrap Kit

I started this project when I was starting the home lab during March 2020, so I kept notes of tools / etc I needed as I went, that's included in the [old ratchet log](old-ratchet/log/ratchet-log.md) and might be worth revisiting someday. 