## Ratchet Devlog 

## 2021 01 08

Pandemic still raging, I am returning to ratchet because machine class is coming up and, despite it not being critical path for any kind of research, I think a large format mill that we can make w/ small scale CNC and 3D Printers is a welcome addition to the family. 

Also, this is fun. A furniture machine / tool changing shopbot replacement. Can basically do 'big clank' approach: ratchet should stick to its roots: easy to fab, pretty cheap, maybe extrusion - and - rollers based, but like 60x120 minimum beam sizes, and end plates in alu or phenolic. ALU is pretty reasonable at scale, you'll want to find a flexure / preload answer though. Maybe it's offset cams this time. I'm looking at extrusion `HFS8-80160` 

This is probably interesting also because i.e. race-to-the-bottom doesn't seem like it's taken off here yet. 

The layout kind of handles itself, but I'm not sure about drive. Big, thick belts are tempting and, yes, this time I should really be into the BLDCs. Perhaps this is two belt-and-pinon drives on YL and YR, and a proper pulley on X. 

ooook, I've started then, dang. I guess since I've just figured it all out I should spend the day getting it into CAD, lest I forget. 

Here's the doodling:

![sketch](2021-01-08_ratchet-sketch.png)
![sketch](2021-01-08_ratchet-sketch.jpg)

Basics:
  - do AT5 or similar belt, ~ 20mm wide, steel core, etc 
  - drives are BLDC, reduced on GT3 / 10mm or so, each does 3-400 watts (?)
  - focus on tensioning
  - bigger rollers for better hertz stress, find calc. for this, calc preload, etc... currently spec'd 6904, no idea if appropriate, want a spreadsheet 
  - X Carriage can be assembled on bench, drops in to YL / YR carriages on M8 
  - YL / YR / X kinematics a-la clank, 
  - (not drawn) should have X carriage w/ well integrated Z, probably worth a lead screw & 'proper' linear rails here 
  - big ol' toolchanger, maybe a-la clank's current, will see 
  - YL face plates are also 'feet' if the machine leans against a wall / is mounted vertically. 
  - simple extrusion frame, but do want provision for vacuum bed... not sure how to do this best 
  - aim at phenolic / epoxy construction for end plates / carriages etc 
  - need preload solution for rollers 
