# Ratchet Dev Log

### Next Rev Notes

This is mostly done, I like the project but I expect when I go again it will be with a few changes.
- extrusions should be double-height, i.e. these are ~ 40x40mm nominally, will go to 80x40.
- belt ends should be static at the extrusion caps, tensioning will happen in the reduction assembly, similar to how the motor pinion-pulleys tension against the drive wheel.
- the motors will rest 'behind' the carriages, as sketched somewhere in these notes... and more care will be taken to present a friendly mounting surface on the 'face' of the carriages.
- the chassis / bed should be much deeper, on order of the size of the overhang on the corner elements, to make sturdier corners. the corner elements should be carefully rethought
- the thing really wants BLDC drive. since this is good enough as it stands, I should make sure I don't come back to hardware design before I have a nicely implemented BLDC driver.
- the z-axis is nice in principle but needs touchups, and should either be fully integrated with a carriage / x-axis or carefully disentangled from it, all modular like

### Assembly Notes / Next Rev Notes

- bigger extrusions, lighter, and closed.
- (note from 2020 04 06) a better carriage, with the motor 'behind' the extrusion, and a bottom 'plate' either 3D printed or milled from phenolic (depending on tools available, phenolic better)
- in the 'intermediary' between the large table and the mechanical bits,
  - this is the awkward part: the corner braces can be done intelligently, but are not
  - most notably, the depth of the table should be such that it is taller than the width of the overhang. the corner brace elements should be *also* useful tools for laminating the table section
  - the table should be *plywood* sheets not MDF: MDF to face, though.
  - sides should be different: one is fixed, only one is the floating side. floating side adjustment also could be intelligent, but these little tiny M3 hex-inserts and slots are *not the right scale* for this thing
  - assembly here should be more heat-set inserts: fishing hex-nuts into nooks here is a PITA - this might not be true were they ~ M4 or M6 hex-nuts, and easy to accesss.
  - generally, a better scheme for assembling gantries (sub assemblies) (belt ends, tension) and then locking those into super-assemblies. for example, depending on the machine, we might want a reduction drive or a speedy drive: dropping different gantries into the same frame would be awesome.

## Bootstrap Kit

I'd also like this project to include notes on the beginner BOM - my isolation BOM - for similar projects later. What do you need to get started producing machines with your machine? Include all hand tools, hardware kit start, etc: drill / driver and circular saw, etc. Saw horses and working table, materials for. Is this the ~ 4k shop? Less? Include the printer, etc.

## Power Tools 

There's just a few of these that a shop shouldn't be without. The first is a cordless drill, and I really appreciate having a 'driver' as well - these typically come in a set. The drill has a chuck for variable diameter drill bits, and an 'impact driver' has a quick-swap endpoint, typically a 1/4" hex shank, or an 1/4 or 1/2" square socket, if you're an auto shop. The impact hammer has a [hammer clutch](https://en.wikipedia.org/wiki/Impact_wrench) that drives screws / etc home with a satisfying oomf. Typically these are used together, so having a drill-bit and a driving-bit pair in two tools makes for faster work. I've included bits and drill bits here that should be found alongside these tools.

I've also included a mitre saw, which is important for cutting lengths of stock (i.e. aluminum extrusions, 2x4s, etc), and a circular saw, along with a long level / straight edge - this can be used in lieu of a table saw to panlize sheet stock. 

Since I'm pulling costs here from McMaster, they're well inflated. Most of these can be found for less at a local hardware store.

Keep in mind that the mitre saw demands a table to live on.

| Tool | Link / McMaster PN | Cost |
| --- | --- | --- |
| Cordless Drill | [29835A116](https://www.mcmaster.com/29835A116) | 196 |
| Cordless Driver | [53145A83](https://www.mcmaster.com/53145A83) | 216 |
| Drill Bits Metric | [2788A12](https://www.mcmaster.com/2788A12) | 80 |
| Drill Bits SAE | [8907A14](https://www.mcmaster.com/8907A14) | 76 |
| Countersink Bit | [3098A35](https://www.mcmaster.com/3098A35) | 14 |
| Driver Bits Robinson / Square | [7021A39](https://www.mcmaster.com/7021A39) | 12 |
| Driver Bits Philips | [2596N2](https://www.mcmaster.com/2596N2) | 11 | 
| Driver Bits Metric Hex | [5492A92](https://www.mcmaster.com/5492A92) | 31 |
| Driver Bits Torx (fancy) | [7396A81](https://www.mcmaster.com/) | 38 |
| Mitre Saw | [41325A18](https://www.mcmaster.com/41325A18) | 351 |
| Circular Saw | [24705A73](https://www.mcmaster.com/24705A73) | 273 |
| 48" Level | [2188A23](https://www.mcmaster.com/2188A23) | 52 | 

## Clamps

It's hard to know what size of clamps you'll need, but any glue-up job demands a handful. If you've been through a woodshop you'll notice there's often an entire wall or so worth of clamps in varying length and flavour, for good reason. This is a set I use often, especially during layup of phenolic sheet structures, and while laminating sheets for the chassis of the Ratchet machine. I won't put quanitities down here, but the more the better.

| Tool | Link / McMaster PN | Cost |
| --- | --- | --- |
| 4-1/2" Clamp | [51755A2](https://mcmaster.com/51755A2) | 5 |
| 6" Clamp | [51755A6](https://mcmaster.com/51755A6) | 22 |
| 12" Clamp | [51755A11](https://mcmaster.com/51755A11) | 30 |
| 24" Clamp | [51755A12](https://mcmaster.com/51755A12) | 32 | 

## Fast Furniture 

For cheap, quick tables, some 2x4's, sawhorse brackets, one can't do much better than a panel on top of two sawhorses. On construction sites, it's not hard to catch the occasional door being used in this manner. 

These are the kit I use for the Ratchet, and I think they're swell. I added a diagonal brace for some shear strength. 

![ratchet](bootstrap/2020-05-31_ratchet-hip.jpg)
![sawhorses](bootstrap/2020-06-01_sawhorse-brackets.jpg)

| Part | Link | Cost |
| --- | --- | --- |
| Sawhorse Brackets | [Amazon: Sawhorse Brackets](https://www.amazon.com/2x4basics-90196-Custom-Brackets-Sawhorse/dp/B0030T1BRY/) | 30 / table |

I'll leave you up to finding 2x4's and table sheets.

## Hand Tools

What follows is my bare basic set of hand tools. This is what I'll take on fab lab installs, and what I've used during my time in quarantine in the 'home shop' - I've actually rarely wanted anything else, but I've also been working in a narrow set of constraints, making the ratchet machine. 

Some notes: first, I love vernier calipers. You could too. Vernier calipers are cool. Second, the scissors here have become one of my cherished objects: these things have lasted me through 4 years of architecture school, multiple internships, and three years at MIT. I have three spares just in case they stop making them. Love your tools. Your tools are your friends. 

| Tool | Link / McMaster PN | Cost | 
| --- | --- | --- |
| Calipers | [2272A21](https://mcmaster.com/2272A21) | 26 |
| Measuring Tape | [19175A47](https://mcmaster.com/19175A47) | 16 |
| Metric Hex Set | [7813A13](https://mcmaster.com/7813A13) | 16 |
| SAE Hex Set | [7813A11](https://mcmaster.com/7813A11) | 15 |
| Multi Screwdriver | [Picquic Sixpac Plus](https://www.amazon.com/Picquic-44106-Dynamic-SIXPAC-STUBBY/dp/B07BCLHL15/) | 21 |
| Side Cutters / Snips | [5372A4](https://mcmaster.com/5372A4) | 32 | 
| Straight Pliers | [5783A2](https://mcmaster.com/5783A2) | 23 |
| Angled Pliers | [5632A16](https://mcmaster.com/5632A16) | 36 |
| Pliers-style Adjustabe Wrench | [51505A21](https://mcmaster.com/51505A21) | 59 |
| Vise Grips | [7136A21](https://mcmaster.com/7136A21) | 14 |
| Decent Scissors | [Olfa Model 9766](https://www.amazon.com/OLFA-Scissors-Stainless-Steel-Serrated/dp/B001D3G51G/) | 16 |
| 18mm Blade Utility Knife | [39585A65](https://mcmaster.com/39585A65) | 14 |
| 9mm Blade Utility Knife | [38155A7](https://mcmaster.com/38155A7) | 9 | 
| Deburring Tool | [4286A16](https://mcmaster.com/4286A16) | 9 | 
| 7mm Wrench (3DP Nozzle Sized) | [7191A51](https://mcmaster.com/7191A51) | 9 |

## Hardware Basic M&S

This is a list of hardware I *assume* I'll have handy at all time. Your own personal hardware set / preference is yours to discover: some of us like tabs, some of us spaces. This is of course also hugely project dependent, but I'll try to include most everything that we commonly find in use in machine projects. 

I tend to buy 18-8 stainless, but many hardcore mech-e's prefer black oxide (which has higher hardness, but can corrode) or zinc-coated steel, which will corrode much slower but is more expensive than 18-8 or BO. I choose 18-8 because in most cases I am fastening 3D Printed (FDM / Plastic) components. 

**SHCS** refers to Socket Head Cap Screws, which are normally the first pick. Length is measured from the bottom of the head to the tip.

![shcs-dwg](bootstrap/shcs-dwg.gif)

**FHCS** refers to Flat Head Cap Screws, which are appropriate when a flush surface is desired, or hardware *position* is critical: the tapered insert asserts a particular position for the screw. I.E. do not use where adjustment is desired. Length is measured from the *top* of the head to the tip.

![fhcs-dwg](bootstrap/bhcs-dwg.gif)

**BHCS** refers to Button Head Cap Screws, which are a nice lower-profile alternative to SHCS, but with reduced drive size, so are easier to strip heads out of. Length is measured from the bottom of the head to the tip.

![bhcs-dwg](bootstrap/fhcs-dwg.gif)

Low-Profile SHCS **LPSHCS** exist, and some engineers keep kits of these instead of standard SHCS, but IMHO if your designs are so sensitive that you are having to use LPSHCS where a button head would do, you are either sending your payloads into space (in which case, congratulations) or you have put yourself into a pinch that some flexible design decisions could have avoided. For reproducible designs, this feels like the spiritual equivalent of a security-torx bit.

| Part | Lengths | Catalog Page |
| --- | --- | --- |
| M3 x 0.5 SHCS | 5, 8, 10, 16, 20, 25, 30, 40, 50, 70 | [M3 SHCS Catalog](https://www.mcmaster.com/shcs/thread-size~m3/18-8-stainless-steel-socket-head-screws-11/) |
| M3 x 0.5 FHCS | 5, 8, 10, 16, 20, 25, 30, 40, 50 | [M3 FHCS Catalog](https://www.mcmaster.com/stainless-steel-hex-drive-flat-head-screws/metric-18-8-stainless-steel-hex-drive-flat-head-screws/) |
| M3 x 0.5 BHCS | 5, 10 | [M3 BHCS Catalog](https://www.mcmaster.com/button-head-cap-screws/thread-size~m3/) |

SHCS and BHCS should typically be used with washers, less so with BHCS, which have a wider head. I'm including nylon lock-nuts or 'nylocks' here as well: the nylon insert in these prevents screws from vibrating loose over time - an important detail in machines actuated with stepper drives that, well, drive steps (vibrations) into everything!

| Part | Link / McMaster PN |
| --- | --- |
| M3 Washers | [93475A210](https://www.mcmaster.com/93475A210) |
| M3 Nylocks | [93625A100](https://www.mcmaster.com/93625A100) |

M3s are somehow the standard issue size for most machine work at the scale presented here. A similar M4 and M6 set would be encouraged for larger machines, as well as the occasional M2. Many of my new designs will use M4 hardwre throughout, for example. Hopefully I'll update this as that work goes on, and I pick out a set. 

What you want here is a kit. A box. Here are the good boxes.

| Part | Link / McMaster PN |
| --- | --- |
| 11 x 6-1/2" Small-Parts Cabinet | [45505T9](https://mcmaster.com/45505T9) |
| Do 18 compartments for parts less-than 25mm | | 
| Do 12 compartments for parts greater-than 25mm | |

## Materials Basic M&S

This is entirely based on the work you will be doing. Recently, I gravitate towards phenolic structures, because it's easy to mill and fairly well performing. Additionally, phenolic sheets tend to have good surface hardness, making them decent bearing surfaces.

| Material | Young's Modulus (GPA) | Specific Young's | Cost for 6mm x 24x24" | Machinability |
| --- | ---: | ---: | --- | --- |
| ABS | 2 | ? | 52 | Not Dimensionally Stable, but OK to Machine |
| Nylon 6 | 3 | 2.5 | 130 | Painful |
| HDPE | 1 | ? | 23 | Easy, Breezy |
| Acetal (Delrin) | 2.8 | ? | 89 | Breezy, also lasers, and non-cracking |
| Cast Acrylic | 3.3 | 2.8 | 46 | Breezy, esp. w/ Lasers |
| 6061 ALU | 69 | 22 | 87 | Breezy with WJ, Painful on Shopbot |
| FR1/CE (Canvas / Phenolic) | 6 | ? | 81 | Excellent (no waterjet) |
| FR4/G10 (Fiberglass) | 22 | ? | 98 | OK (no waterjet) |

I will tend to assume I have 1/4" sheets of acrylic on hand, 12x24" at least (or whatever fits the laser bed), as well as 24x36" sheets of 3/16" phenolic. In a perfect world I would also have an endless supply of 1/4, 1/2 and 3/4" baltic birch high-quality plywood 4x8's, but it's really more appropriate to order for your projects.

Additionally, choosing the right printing filament is open debate in the 3DP world. I've moved towards using 'Tough' PLAs, these are PLAs with some glycol added to make them a bit more, well, tough. Keep in mind you do sacrafice some stiffness for this, but the inter-layer adhesion is also better and the flexural allowance is great, for a small loss. Besides, plastics are (big reductive statement) floating around 3GPa flexural modulus anyways, so a little +/- 0.5GPa isn't so consequential. I besides try not to design any plastic parts that do much structural work. 

| Filament Pick | Link |
| --- | --- |
| 'Pro' Tough PLA | [MatterHackers](https://www.matterhackers.com/store/l/gray-pro-series-tough-pla-filament-175mm-1kg/sk/MJJWMY5G)

## Mechatronic Basic M&S

Transmissions, transmissions, etc. These are common components I assume exist in the shop, for making linear stages, rotary stages, reductions, etc. 

| Part | Link / PN | Notes |
| --- | --- | --- |
| GT2 Belt 6mm Width Open Ended | [Amazon](https://www.amazon.com/Meters-Timing-Fiberglass-Printer-LINGLONG/dp/B07MZLFPQJ/) |
| GT2 Belt 6mm Width 280mm Closed | [Amazon](https://www.amazon.com/280-2GT-6-Timing-Belt-Closed-Loop/dp/B014SLWP68/) | 
| HTD 3M Belt 15mm Width Open Ended | [Amazon](https://www.amazon.com/HTD-3M-Timing-Belt-Engraving/dp/B017S900KA/) | 
| 624 4x13x5 Bearings | [VXB: 10 Count](https://www.vxb.com/624ZZ-Shielded-4x13x5-Miniature-Bearing-Pack-of-10-p/kit843.htm) |
| 608 (skate) Bearings | [VXB: 200 Count](https://www.vxb.com/200-608ZZ-Shielded-8x22x7-Skateboard-Bearing-p/608-zz-skateboard-bearing.htm) |
| 6806 (bb30) Bearings | [VXB: 1](https://www.vxb.com/6806-2RS-Bearing-Deep-Groove-6806-2RS-p/6806-2rsbearing.htm) or [Amazon: 10](https://www.amazon.com/uxcell-6806-2RS-Groove-Bearings-Double/dp/B082PXK5K9/) |
| 40x52x7 Angular Contact (headset) Bearings | [VXB](https://www.vxb.com/1-1-2-sealed-Bicycle-Headset-Bearing-40x52x7mm-p/38-7-bicycle-headset-bearing.htm) | 
| 30.15x41.8x7 Angular Contact (headset) Bearings | [VXB](https://www.vxb.com/sealed-Bicycle-Headset-Bearing-45-45-p/7-45-bicycle-headset-bearing.htm) | Nominally '1-1/8" Headset' bearings, some variance exists, order then measure. |
| 8mm OD 400mm L Linear Shaft | [McMaster](https://www.mcmaster.com/6112K45) | Often cheaper elsewhere: amazon, ebay, alibaba, etc |
| 12mm OD 400mm L Linear Shaft | [McMaster](https://www.mcmaster.com/6112K104) | as above, | 
| T8 Lead Screw | [Amazon](https://www.amazon.com/Copper-Straight-Stainless-Printer-MTFX/dp/B07W8M4ZSR/) | *not* high performance, expect some slop
| NEMA 17 Motors 42x42x48mm | [OMC-StepperOnline](https://www.omc-stepperonline.com/nema-17-stepper-motor/5pcs-of-nema-17-bipolar-59ncm-84oz-in-2a-42x48mm-4-wires-w-1m-cable-and-connector.html) |
| NEMA 23 Motors 57x57x56mm | [OMC-StepperOnline](https://www.omc-stepperonline.com/nema-23-stepper-motor/p-series-nema-23-bipolar-1-8deg-1-26nm-178-4oz-in-2-8a-2-5v-57x57x56mm-4-wires.html) |

For tape chain assemblies (e-chain), I use:

| Part | Link | Notes |
| --- | --- | --- |
| Fatmax Tape Measure | [McMaster](https://www.mcmaster.com/6817A2) | Forms chain 'spine' |
| 1" ID Expandable PET Braided Sleeving | [McMaster](https://www.mcmaster.com/2837K76-2837K34) | Wrap cable / tape with this |
| ~ 30mm Diameter 3:1 Heat Shrink Tubing | [McMaster](https://www.mcmaster.com/7861K57) or [Amazon](https://www.amazon.com/XHF2018-Dual-Wall-Shrink-Tubing/dp/B07HT3RK35/) | Terminate ends with this |

## Electronics Tools

| Tool | Link | Notes |
| --- | --- | --- | 
| Wire Strippers | [DigiKey](https://www.digikey.com/product-detail/en/klein-tools-inc/11063W/1742-1381-ND/6804883) or [Sparkfun](https://www.digikey.com/product-detail/en/sparkfun-electronics/TOL-14872/1568-1923-ND/9644298) |
| Soldering Station | [DigiKey](https://www.digikey.com/product-detail/en/apex-tool-group/WE1010NA/WE1010NA-ND/8017918) | | 
| Heat Gun: Rework and Heat Shrink | [Digikey](https://www.digikey.com/product-detail/en/apex-tool-group/6966C/6966C-ND/4502205) | |
| Heat Gun: Heat Shrink | [Amazon](https://www.amazon.com/BLACK-DECKER-HG1300-Dual-Temperature/dp/B004NDX7O6/) | |
| Logic Analyzer (fancy) | [Saleae](https://www.saleae.com/) | |
| Multimeter: Fluke 115 | [DigiKey](https://www.digikey.com/product-detail/en/fluke-electronics/FLUKE-115/614-1010-ND/1506332) | |

## Electronics Basic M&S

| Part | Link / DigiKey PN | Notes |
| --- | --- | --- |
| Zip Ties 6" | 1436-1522-ND | |
| 50ft 18AWG Silicone Red | CN101R-50-ND‎ | |
| 50ft 18AWG Silicone Black | CN101B-50-ND‎ | | 
| 50ft 18AWG Silicone Orange | CN101A-50-ND‎ | |
| 22AWG Solid-core Wire Kit | 1568-1357-ND  | |
| Perma-Proto Boards | 1528-1147-ND | |
| Solder 18AWG | KE1105-ND |
| Solder 24AWG | NC2SW.0201LB-ND |
| PSU 350W 24V | 1866-3346-ND |
| PSU 1000W 24V | 1866-4217-ND |
| PSU Power Entry | 486-3979-ND |
| PSU Power Entry Fuses | 486-1226-ND‎ |

## 2021 01 08

Pandemic still raging, I am returning to ratchet because machine class is coming up and, despite it not being critical path for any kind of research, I think a large format mill that we can make w/ small scale CNC and 3D Printers is a welcome addition to the family. 

Also, this is fun. A furniture machine / tool changing shopbot replacement. Can basically do 'big clank' approach: ratchet should stick to its roots: easy to fab, pretty cheap, maybe extrusion - and - rollers based, but like 60x120 minimum beam sizes, and end plates in alu or phenolic. ALU is pretty reasonable at scale, you'll want to find a flexure / preload answer though. Maybe it's offset cams this time. I'm looking at extrusion `HFS8-80160` 

This is probably interesting also because i.e. race-to-the-bottom doesn't seem like it's taken off here yet. 

The layout kind of handles itself, but I'm not sure about drive. Big, thick belts are tempting and, yes, this time I should really be into the BLDCs. Perhaps this is two belt-and-pinon drives on YL and YR, and a proper pulley on X. 

ooook, I've started then, dang. I guess since I've just figured it all out I should spend the day getting it into CAD, lest I forget. 

Here's the doodling:

![sketch](2021-01-08_ratchet-sketch.png)
![sketch](2021-01-08_ratchet-sketch.jpg)

Basics:
  - do AT5 or similar belt, ~ 20mm wide, steel core, etc 
  - drives are BLDC, reduced on GT3 / 10mm or so, each does 3-400 watts (?)
  - focus on tensioning
  - bigger rollers for better hertz stress, find calc. for this, calc preload, etc... currently spec'd 6904, no idea if appropriate, want a spreadsheet 
  - X Carriage can be assembled on bench, drops in to YL / YR carriages on M8 
  - YL / YR / X kinematics a-la clank, 
  - (not drawn) should have X carriage w/ well integrated Z, probably worth a lead screw & 'proper' linear rails here 
  - big ol' toolchanger, maybe a-la clank's current, will see 
  - YL face plates are also 'feet' if the machine leans against a wall / is mounted vertically. 
  - simple extrusion frame, but do want provision for vacuum bed... not sure how to do this best 
  - aim at phenolic / epoxy construction for end plates / carriages etc 
  - need preload solution for rollers 

## 2020 05 31

![img](2020-05-31_ratchet-hip.jpg)
![img](2020-05-31_ratchet-overhead.jpg)
![img](2020-05-31_ratchet-z.jpg)
![img](2020-05-31_ratchet-yl.jpg)

Last notes on the z-axis: the lead-screw tie-off needs to happen at *both* points, and you need to test print for proper thread matching. In general, the z-plates, in quarters, feel as though there could be a better way. But I don't think it's worth the effort... just tie things off properly, re-print the assy, you're done here.

My final thoughts on the machine itself, gantries etc, is that this size drive is perhaps overkill for this size extrusion.

My main design errors / ease of assembly errors / were in making the points of contact between gantries (i.e. the two y-gantries meeting the x-gantry) cumbersome to assemble. Also where the z- and x- carriages meet, the final set up is about as stiff as it could be with 3D printed components, but is in no way elegant.

So there would be two viable directions for a re-do.

- one is to match beam stiffnesses to the drive, either with bigger extrusions or, ideally, big laid up beams.

- the other is to get smaller, and use pulley drives to reduce travelling mass: this would make the motion system more akin to a big laser, and would probably *not* be suited to 6mm GT2, but might be suited to 9mm (wide) GT2. this would be a big reduction in stiffness but perhaps would make the system more appropriately spec'd for the tools used in this size end effector. I struggle to imagine GT2 belts doing well at all over this size machine though

to clarify, I should try to recall what part of machine parameter space this is meant to occupy... of course, it should be light, stiff, fast, cheap, and silent (haha)

As it stands this feels a bit over-built, but I think also for the task of ripping through, eventually, 1" thick plywood sheets, it's totally appropriate.

So, broadly, I would re-do this, but would try to greatly improve the carriage by going to 625zz's with M4 thru axels, as is the plan for Clank, and I would try to design around an assumption of sheet cutting, as often as possible, as is also the plan with Clank. Printing is always a viable replacement for sheets, versions could be spun. I would go to 2x1 size extrusions in both axis, and 'flip' the pinion so that it rides 'behind' the 'face' of the carriage. I would make the pinion the site-of-tensioning via a rotation, so that end plates and carriage faces could be snugged up against one another.

## 2020 05 14

Through the z-design today / yesterday. A real PITA - I like the notion here, but next time it would be wiser to work from the z- to the x- axis ... these are *not* going to be easy to put together as they're quite strongly linked. That means also will be difficult to take apart. Whatever. Ratchet version 1 < Rachet version 2.

![zaxis](2020-05-14_zaxis.png)

## 2020 05 11

After some time ignoring this project (working on OSAP), I'm trying to get it assembled. This is going OK, most of my frustrations are with the corner braces.

![rass](2020-05-11_ratchet-assembly-01.jpg)
![rass](2020-05-11_ratchet-assembly-02.jpg)
![rass](2020-05-11_ratchet-assembly-03.jpg)
![rass](2020-05-11_ratchet-assembly-04.jpg)

## 2020 04 14

It occurred to me today, sitting in group lunch but daydreaming about a 4x8 machine, that I should (for that size) go to N23 and hang the motor around the back, using parts that *could* be printed but would also be well suited to flat-stock...

![hangout](2020-04-14_4x8-ratchet.jpg)

This time there's a 'tall' axis, the motor and reduction hang past this bottom plate (mostly I am thinking about hot motors) and 3d printed inserts to go between. At scale, those are laser cut alu plates and some smaller billet components. OK. Also - brushless motors are cooler than this, though it's nice.

## 2020 04 04

OK, I think I'm going to finally get through the XY CAD for this today... it would be awesome to touch some code later on.

Maybe the *very* last thing would be adding some way to put some BFCs on the panel panel. I've done this... Now, the assy..

![render](2020-04-04_ratchet-render.jpg)

OK, all looks well. I guess I'm ... done here for now? Incremental will just be waking up motors, controllers, code, and keeping the printer fed with GCodes and Filament.

## 2020 04 03

Continued efforts to try and finish CAD for phase one.

I think I just have one thing left, before I'm up to ahn xy- assembly for this thing. I'll do that after dinner, and then roll it together in rhino (for mirrors, etc). Later tonight, I'll prep the next print - I figure I should make the reciprocal base-plate component for the 30h thing coming off now.

## 2020 04 02

I've been trying out the big nozzle, and I think it actually doesn't really hit well enough for most bits, I really just want it on the big friggen corners. Since it's on the machine now, I should get those corners cooking, first. I am also cooking dinner at the moment. Quarantine is weird!

For 12-Perimeter webs, 9.86mm thick.

OK, numbers for the nozzles. With 0.8, 0.4 layer at 10mm cubed / s, 1400g filament in 1d10h. With 0.4, 0.3 layer, 1150g in 1d20h. A more matched plastic use, 1310g, 2d3h.

OK, working on the cable cradle now. I will just include one bus-drop thing here, because I can put a connector inline just using headers and IDCs. Nice. IDCs score some ease-of-use points.

OK, left is the bottom-left entry point / cable route take-off for the PSU (kw!), power entry, router, etc ... those can each be unique little things, real deal is just about the cable entry, maybe that's just one of the post-caps...

## 2020 04 01

I'd like to more or less finish the CAD for 'the rest' of this machine today. That means everything up to the z-axis and end effectors. First, for wiring: I'll attach a router / PSU thing on the bottom-left corner (near assumed 0,0), and anchor my cable chain assembly here. I think this means I need to finish the corner posts first. I'll run one bus-line and one other-line (idc) out from here, including ahn power set of gnd, 24, and 5v. That's one component that I guess I'll integrate with one of the extrusion-caps on the corner. At the left-y-carriage, I'll break this out with another part: the clamshell-to-line breakout, which integrates (1) ahn fan, (2) the bus dropper, (3) the osns adaptor. I'll put also some affordances here for 18ga wire-with-bullets. Then, finally, those carriage-to-beam mounts, having one- and two- tape chain mounts, at level with the previously mentioned part. Just standoffs here, no termination.

So,
- corner posts, corner post cap wiring entry point
- clamshell sheet loop mount having: (1) the bus dropper and affordances for a power kit, (2) another bus dropper (just to disconnect the second line...?), (3) a fan and (4) the osns adaptor. (5) this protects motor stuff from rubbing on the tape chain loop, adjacent
- x-to-y mounts, one having two, and one having one, standoffs to accept tape chain stuff. just ends.

That's not so bad!

Probably I'll start with the flag-on-the-motor thing...

## 2020 03 30

Just thinking of printing those big corners now,
- is filament here?
- nozzles: do 1mm, and use variable layer heights around circular outlines? or coordinate to print bottom-up ?
- 1mm nozzle ... yeh
- 1mm nozzle w/ 0.6mm layer height, design for:
 - 8.1mm print-plane wide slots
 - 6mm tall webs (5 solid layers top/bottom)

## 2020 03 29

Through final assembly of one DOF today, feels a bit sticky, I think maybe the preload is a tad high, and now that I've cinched the belt up, there's some additional friction in the belt / drive gear bearings as well.

I guess today I should turn this thing on, before I start along printing more pieces... I can do that all with existing code, and see about acceleration limits. Would be cool to have some motor stall code...

- might want to also pinch ratio a bit, to increase motor leverage ? shame I'm already printing the viscera

Some notes on assembly order,

### Assembly Order

Clamshell Together: Thru Bearings, Thru Viscera, Thru Quad.  
Top Plate to Clamshell: 4 Binders  
Interior Plate to Clamshell: Thru Bearings, 4 Thru  
Interior Plate to Bottom Clamshell: Thru Bearings, 2 Thru  

| Assembly Step | Part | QTY |
| --- | --- | --- |
| Clamshell Thru Idlers | M3 FHCS 30 | 2 |
| Clamshell Thru | M3 FHCS 25 | 4 |
| Clamshell Thru Viscera | M3 FHCS 50 | 4 |
| Top Plate to Clamshell Binders | M3 FHCS 25 | 4 |
| Clamshell Thru to Interior Plate Bearing Seat | M3 FHCS 30 | 2 |
| Clamshell Thru to Interior Plate | M3 FHCS 50 | 4 |
| Interior Plate to Bottom Clamshell Thru and Bearings | M3 FHCS 35 | 4 |
| Top Plate Thru Interior Plate Top Bearings | M3 FHCS 25 | 2 |
| Clamshell Motor Mounts | M3 FHCS 8 | 4 |
| Belt Tension End | M3 FHCS 10 | 4 |

| Revision HW | Part | QTY / DOF |
| --- | --- | --- |
| Tension Push-on | M6 FHCS 10 | 8 |

| Part | Total QTY / DOF |
| --- | --- |
| M3 FHCS 8 | 4 |
| M3 FHCS 10 | 4 |
| M3 FHCS 25 | 12 |
| M3 FHCS 30 | 4 |
| M3 FHCS 50 | 8 |

## 2020 03 27

Today! Assembly...

One thing I'm going to change is the exterior plate. I'm going to turn this into two revolves, which I can print face-up, rather than this awkward and long sideways thing. Effectively this just needs to be a big standoff.

![es](2020-03-27_exterior-standoffs.png)

I can mostly continue and try to assemble one DOF now. Maybe tomorrow's exercise would be waking up a new controller for that... towards pendulum hello world.

Made some adjustments - to belt tensioners, and adding a flange on the drive gear. Those will print this afternoon and overnight, and then tomorrow I can carry on with assembly, and wake up some electronics.

Today I might also design some test axis cradles, and do a hardware count.

 - I should count hardware to make sure I have enough for the machine
 - axis cradles / stands

Thinking more of the board standoffs, I think the x-distance is really set with the x beam, I will need some adjustment (potentially ~ 10mm or more) in the y-beam ends, on one side... I should get to this once I wake one axis up, completely.

Another thought: I should get ahn 0.8mm nozzle... looks like Prusa only wants up to 0.6mm for the machine, though I know Sam has done 0.8. Going to 0.6 turns a 1d15h print into a 1d1h print, for similar material use. I'd bet that going to 0.8 does even more damage. Wonder if that's worth it. Yep, 0.8 goes to 18h. I expect this is ... probably worth it? I've ordered some nozzles, will see.

![assy](2020-03-27_ratchet-assembly.jpg)

Last rights for today, I'll design some axis stands, for desktop tests. I know I'll also need to start handling cables, oy! I suppose it would be prudent to order some sheathing and tape measures... I wonder if there's a simpler way? Travel distances are long here.

Quick CAD things I want:
 - axis cradles
 - BB30 Assy Tool (OD-press-in w/ id-locator)
 - BB30 Removal Tool (id-press-out)
 - bins!
 - Hex Insert-er
  - first tool has nub to align / push,
  - second is just a handle on a long M3 SHCS, use head to push down

## 2020 03 26

Worked through some detail design on the corner members today.

![corner](2020-03-26_corner.png)

But looking at printing these is tough. I'm used to ~ 9hr prints max, this (I need four of them) is ~ 1d15h twice, so 12 days to print everything. Also, each is about 1kg of plastic (seems like an over-estimate to me: prusa slicer?).

Given the relative cost of that, I think I can pinch in that long axis, maybe the post as well. I figure I'll spend one more session - maybe tonight (?) - trying to whittle this down. Perhaps this means adding some more intentional webbing in as well... some macro internal structure rather than just relying on sparse infill. Big shrug, who knows.

Also! I should get the ratchet strap before I finalize this: I'll want to measure the hook. Alternately, I could cede on tying a reef knot in the strap someplace, and really reduce the size of that hole.

Yeah, these will be a pain no matter what, but I think some medium-intelligent webbing will decrease that pain. Bit of a CAD PITA.

## 2020 03 25

Should try to work through the tensioning devices today. This proves a bit tough, as I need to somehow terminate the extrusion as well. That, and mounting to the other carriage / etc, should all roll into one device... It's all a bit awkward. Started on this last night...

... use screws-into-rail to secure, just pinch

I think I've worked this with a *very* simple piece of endmatter: a cap to the extrusion, largely held on with belt tension, where the belt curves around to be pulled 'down' with a clamp that has some inserts for pushing away...

![em](2020-03-25_endmatter.png)

The cap can be integrated with whatever mounts the extrusion to the next piece: carriage or board support. On one end, the clamping element can be integrated with the cap, to simplify and provide sturdier clamping. This will allow me to tension axis even when the cap is flush against another surface, as in the carriage-carriage connection.

![insitu](2020-03-25_endmatter-context.png)

This aligns well with other clearances. For posterity, in the future I can add M6 SHCS in the cap, to land the M3 tensioning elements: this way I won't be marring the plastic as severly.

I can also add some clamping element on the extrusion, so that there's less slop there.

OK, slop reduced, detailing added:

![emd](2020-03-25_endmatter-detail.png)

Now, the board clamp. This will be more or less the same, but with the reach around. And I can now figure what the reach-over length should be. I'd like to be able to face the full width of whatever board the thing is attached to.

So, that overhang from extrusion center to end-plate center is *145.6mm*

This will make for some long members:

![over](2020-03-25_layup.png)

So those will be chunky prints. I expect I'll do them near solid, maybe these are worth webbing out. I should add + 5mm on either side as well, for 150mm center-to-edge. Whoahwee. I think I can get away with reducing the flange on the connector by 5mm, for 145 total now. Not a monumental difference, but every pinch helps.

OK, so clamping to the table seems tough to do well. I will be relying on some monolith prints here, which is too bad, but seems to be the bootstrapping way. It occurred to me to use a ratchet strap (convenient name alignment - maybe I'll call it Ratchet) to cinch the corner members against one another.

![strap](2020-03-25_corner-strap.png)

Here I'm favouring size on the corner element to ween some out of the extrusion carrying member, but perhaps I should rethink this ... so the corner just elegantly tucks the layup together, and then other things can bolt on. This way, we could have versions where the width is 'full width' for edge milling, and others that are more reasonable for some ~ 3.5' widths... and whatever other sized machines we'd like.

To go wider, in any case, I think I'd still like more depth. This image shows two interior laminations on 5/8" sheet, maybe three is the move. Hopefully I can finish this off tomorrow. Tonight I'll put the printer on making end matter for the X axis.

Just ...
 - clamp to table device, integrating this.

Then I would be on to other tasks,
 - the z axis (!)
 - printing some storage buckets
 - making ahn pendulum arm
 - making ahn knife tool

## 2020 03 24

Still waiting on hardware to test this thing, CAD remaining is:

- belt end, belt tension.
- parts storage bucket
- pendulum arm / sensor (dang: should have digikey'd that)
- end plates / beam to beam
- table / machine to table
- end plates / gantry to tool

I think the move is to make bed-to-gantry mounts out of Printed Bits.

![b2m](2020-03-24_board-to-machine.jpg)

This way making machine chassis is wicked simple: we just get a big thick piece of plywood / mdf / similar substrate. These will take a tonne of time to print (likely) but I have that, and they'll successfully align the tool to the sheet in Z and in Parallel-Ness (taking the factory parallel from the board).

I can integrate belt tensioning work here.

To go forward - I should get some of these printing - I need to figure out what the axis-to-other-axis relationship is like, as well as what the height offset wants to be, etc.

## 2020 03 22

Good CAD day yesterday, onto others today. If the plan is to knife-cut much PET, I think a hold down vac will be a big tool. Not sure how well this can really work w/ a shop-vac, but [here](https://www.amazon.ca/PowerSmith-PAVC101-10-Amp-Vacuum-Green/dp/B0060EUA32/) is a pick for one - 10A actual power, 1.2kW, better than most others.

Also thinking about tables etc a bit today... Merrick is going to get involved there. Some way of making frame w/ one dimension at 4', having components that (maybe an under-cling) mount the y-extrusions onto that surface. Want the tool to go full width of the work area. Would be ideal to be able to swap the machine frame from various legs: might even want to mount the thing to the wall. Work in steps.

## 2020 03 21

Getting through this cage, which is a substantially different topology than my previous LUGs, which win on elegance because they grip *edges* rather than having to reach around an entire extrusion. In any case, I'm working through this, and I think I've hit the ticket:

![cage](2020-03-21_cage-out.png)

I'm pulling long M3's through bearing shafts, whose shoulders are built-in to the printed / milled / moulded bits. There's one of those such bits (plates) per device, plus this 5th face closing the 'clamshell' on the bottom, and there will be one more up top that closes the belt / idler shell.

Those two protrusions on the side-facing-the-camera in the above screen will be places to mount whatever-the-carriage-holds, and should be able to also add some addn'l strength to the parts. Hope is that at end of day each of these components can be 2-sided milled, and reciprocally easily injection molded, if we ever go towards machines-like-drill-guns ubiquity, where extrusion and carriage systems make a tonne of sense.

The clamshell is a bit of a trick to integrate,

![modmod](2020-03-21_clamshell-cage.png)

I think I can skewer those last rollers and retain the consistency of the vertical-pinch component, which I would prefer: prevents the vertical preload error stack going through whatever junction is betwixt the clamshell and the rest of the carriage. Another win for integrated design. The rest of the union might be something of a trick, let's see.

- single top plate sets length but not strength, skewer clamshell for that

OK, think I'm mostly through this...

![clam](2020-03-21_clamshell-cage-close.png)

I think I just want *one* more set through that ear/tag on the upper left in this photo. I can run that through the whole clamshell as well. Then I should detail joinery, and I'll know about hardware.

To proceed with CAD, I figure I should...

- finish the assy w/ bearings, screws, locknuts in place (end)
- sanity check ...
- print inner plate,
- hardware for end pieces, or do hardware kit assuming endless quarantinis

I can also close out w/ a collected hardware kit on McMaster

- 25mm flatheads to hold most of clamshell,
- 30mm flatheads thru the clamshell idler shafts, and the clamshell->interior face reach
- 50mm flatheads to go thru the viscera, thru bottom clamshell and plate
