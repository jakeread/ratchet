# Ratchet

(strap)

[DEV LOG](log/ratchet-log.md) for updates.

This is a bootstrapping machine, aimed towards fabrication of sheet stocks using a routing end-effector, assuming starting equipment is barebones shop tools (listed) and some 3D Printer. The BOM is ideally short and sweet and cheap, and assembly is ideally simple.

![img](log/2020-05-31_ratchet-hip.jpg)
![img](log/2020-05-31_ratchet-overhead.jpg)

## XY-Drive

![img](log/2020-05-31_ratchet-yl.jpg)

XY axis use 3M belts, 15mm width, with steel cores. This means they should be 'stiff-ish' - much better than GT2 belts common to most open source 3D Printers. These are driven with a NEMA 17 motor at a 5:1 reduction for bonus torque and holding force.

Carriages all use preloaded 608zz skate bearings, which are available ~ $200 for 1000qty.

## Z-Drive

![img](log/2020-05-31_ratchet-z.jpg)

The Z axis uses a 'standard' 8mm lead screw, with a captured lead nut also driven on a belt reduction. Z-throw is short, but supports are tall. This also uses preloaded 608zz bearings, but roll on some linear rails in this case, not extrusion.
